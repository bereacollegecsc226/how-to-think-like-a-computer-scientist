* (Updated 1/12/18) Chapter 1 The way of the program
* (Updated 1/12/18) Chapter 2 Variables, expressions, and statements
* (Updated 1/21/18) Chapter 3 Hello, little turtles!
* (Updated 1/17/18) Chapter 4 Functions
* (Updated 1/30/18) Chapter 5 Conditionals
* (Updated 2/1/18) Chapter 6 Fruitful functions
* (Updated 2/13/18) Chapter 7 Iteration
* (Updated 2/15/18) Chapter 8 Strings
* (Updated 3/9/18) Chapter 9 Tuples
* (Updated 3/22/18) Chapter 10 Event handling
* (Updated 3/9/18) Chapter 11.a Lists - Part A
* (Updated 3/22/18) Chapter 11.b Lists - Part B
* (Needs converted to 3.6) Chapter 12 Modules
* (Updated 3/22/18) Chapter 13 Files
* (Updated 3/9/18; needs review) Chapter 14 List Algorithms
* (Updated; needs review) Chapter 15 Classes and Objects - the Basics
* (Updated; needs review) Chapter 16 Classes and Objects - Digging a little deeper
* Chapter 17 PyGame
* Chapter 18 Recursion
* Chapter 19 Exceptions
* (Updated 04/19/2018) Chapter 20 Dictionaries
* Chapter 21 Even more OOP
* Chapter 22 Collections of Objects
* Chapter 23 Inheritance
* Chapter 24 Linked Lists
* Chapter 25 Stacks
* Chapter 26 Queues
* Chapter 27 Trees
* (TODO) Appendix A Debugging
* Appendix B An odds-and-ends Workbook
* Appendix C Configuring Ubuntu for Python Development
* Appendix D Customizing and Contributing to the Book
* (TODO) Appendix E Some Tips, Tricks, and Common Errors